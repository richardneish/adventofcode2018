use std::io::BufRead;
use std::io::Lines;

pub fn process_input(lines_iter: Lines<impl BufRead>) -> i32 {
    let mut sum: i32 = 0;
    for line in lines_iter.map(|l| l.unwrap()) {
        let number = i32::from_str_radix(&line, 10).unwrap();
        sum += number;
    }
    return sum;
}

#[cfg(test)]
mod tests {
    use day1a::process_input;
    use std::io::BufRead;
    use std::io::Cursor;
    #[test]
    fn day1a_1() {
        let input = Cursor::new(String::from("+1\n-2\n+3\n+1\n")).lines();
        assert_eq!(process_input(input), 3);
    }
    #[test]
    fn day1a_2() {
        let input = Cursor::new(String::from("+1\n+1\n+1\n")).lines();
        assert_eq!(process_input(input), 3);
    }
    #[test]
    fn day1a_3() {
        let input = Cursor::new(String::from("+1\n+1\n-2\n")).lines();
        assert_eq!(process_input(input), 0);
    }
    #[test]
    fn day1a_4() {
        let input = Cursor::new(String::from("-1\n-2\n-3\n")).lines();
        assert_eq!(process_input(input), -6);
    }
}
