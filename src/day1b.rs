use std::collections::HashSet;
use std::io::BufRead;
use std::io::Lines;
use std::vec::Vec;

pub fn process_input(lines_iter: Lines<impl BufRead>) -> i32 {
    let lines: Vec<String> = lines_iter.map(|l| l.unwrap()).collect();

    let mut seen_freqs = HashSet::new();
    let mut freq: i32 = 0;
    seen_freqs.insert(freq.clone());
    'lines: loop {
        for line in &lines {
            let number = i32::from_str_radix(&line, 10).unwrap();
            freq += number;
            if seen_freqs.contains(&freq) {
                break 'lines;
            }
            seen_freqs.insert(freq.clone());
        }
    }
    return freq;
}

#[cfg(test)]
mod tests {

    macro_rules! day1b_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (input, expected) = $value;
                    assert_eq!(expected, process_string(input));
                }
            )*
        }
    }

    use day1b;
    use std::io::BufRead;
    use std::io::Cursor;

    day1b_tests! {
        day1b_1: ("+1\n-2\n+3\n+1", 2),
        day1b_2: ("+1\n-1", 0),
        day1b_3: ("+3\n+3\n+4\n-2\n-4", 10),
        day1b_4: ("-6\n+3\n+8\n+5\n-6", 5),
        day1b_5: ("+7\n+7\n-2\n-7\n-4", 14),
    }

    fn process_string(string: &str) -> i32 {
        let input = Cursor::new(&string).lines();
        return day1b::process_input(input);
    }
}
