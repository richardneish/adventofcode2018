extern crate adventofcode2018;
extern crate structopt;

use adventofcode2018::day1a;
use adventofcode2018::day1b;

use std::io;
use std::io::BufRead;
use structopt::StructOpt;

#[derive(StructOpt)]
struct Opt {
    day: String,
}

fn main() {
    let args = Opt::from_args();

    let stdio = io::stdin();
    let lines_iter = stdio.lock().lines();
    let result = match args.day.as_ref() {
        "day1a" => day1a::process_input(lines_iter),
        "day1b" => day1b::process_input(lines_iter),
        _ => panic!("No match for day"),
    };
    println!("Result: {}", result);
}
